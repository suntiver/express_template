const router = require('express').Router()
const controllers = require('../../controllers/user.controller')
// const auth = require('../auth')
const validator = require('../../validations')

// router.get('/', auth.required, controllers.onGetAll)
// router.get('/:id', auth.required, controllers.onGetById)
// router.post('/', auth.required, controllers.onInsert)
// router.put('/:id', auth.required, controllers.onUpdate)
// router.delete('/:id', auth.required, controllers.onDelete)
router.get('/',  controllers.onGetAll)
router.get('/:id',  controllers.onGetById)
router.post('/',  controllers.onInsert)
router.put('/:id',  controllers.onUpdate)
router.delete('/:id',  controllers.onDelete)
router.post('/login', controllers.onLogin)
router.post('/register', controllers.onRegister)
router.post('/refresh-token', controllers.onRefreshToken)

module.exports = router