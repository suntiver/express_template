const express = require('express'),
      app = express(),
      config = require('./configs/app')

// Express Configs
require('./configs/express')(app)

// Middleware
require('../src/api/v1/middlewares')

// Routes
app.use(require('../src/api/v1/routes'))

// Error handler
require('./configs/errorHandles')(config.isProduction, app)

// Start Server
const server = app.listen( config.port, () => {
  let host = server.address().address
  let port = server.address().port
  console.log(`Server is running at http://${host}:${port}`)
})